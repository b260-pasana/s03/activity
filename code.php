<?php

class Person {

    //Properties
    public $firstName;
    public $middleName;
    public $lastName;

    // Contructor
    public function __construct($firstName, $middleName, $lastName) {
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }

    // Methods
    public function printName() {
        return "Your full name is $this->firstName $this->middleName $this->lastName";
    }

}

class Developer extends Person {

    public function printName() {
        return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer.";
    }

}

class Engineer extends Person {

    public function printName() {
        return "You are an engineer named $this->firstName $this->middleName $this->lastName.";
    }

}

$person = new Person('Madison', 'Pasana', 'Sangalang');
$developer = new Developer('Mariechris', 'Pasana', 'Sangalang');
$engineer = new Engineer('Don', 'Oliveros', 'Sangalang');
