<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S03: Classes and Objects</title>
</head>
<body>

    <h1>Activity Solution</h1>

    <h2>Person</h2>
    <p><?= $person->printName(); ?></p>

    <h2>Developer</h2>
    <p><?= $developer->printName(); ?></p>

    <h2>Engineer</h2>
    <p><?= $engineer->printName(); ?></p>
    
</body>
</html>